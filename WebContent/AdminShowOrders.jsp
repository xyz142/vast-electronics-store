<%@page import="com.hwua.po.Orders"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>用户的订单管理</title>
</head>
<style type="text/css">
 #boxs{
 	margin:0 auto;
 	width: 850px;
 	height: 1200px;
 }
 #boxs th,tr,td{
 	width:150px;
 	border: 1.5px solid #1abc9c;
 	text-align: center; 
 	padding: 15px 15px;
 }
 #top{
 	margin:0 auto;
 	width: 700px;
 	height: 70px;
 	text-align: center; 
 }	
</style>
<body>
	<div id="top">
		<h2 style="color: gray">用户订单</h2>
	</div>    
	<div id="boxs">
	 	<table cellpadding="0" cellspacing="0">
	 		<tr>
		 		<th>订单编号</th>
		 		<th>创建时间</th>
		 		<th>订单状态</th>
		 		<th>商品编号</th>
		 		<th>收货地址</th>
		 		<th>买家编号</th>
		 		<th>发货</th>
		 		<th>删除</th>
	 		</tr>
		 	<%List<Orders> list = (List<Orders>)request.getAttribute("orderslist"); %>
		 	<%for(int i = 0;i<list.size();i++){ %>
	 		<tr>
		 		<td><%=list.get(i).getOid() %></td>
		 		<td><%=list.get(i).getOdate() %></td>
		 		<td><%=list.get(i).getOboolean() %></td>
		 		<td><%=list.get(i).getPid() %></td>
		 		<td><%=list.get(i).getSiteid() %></td>
		 		<td><%=list.get(i).getUserid() %></td>
		 		<td><a href="AdminDoSendOrders?oid=<%=list.get(i).getOid()%>">发货</a></td>
		 		<td><a href="AdminDoDeleteOrders?oid=<%=list.get(i).getOid()%>">删除</a></td>
	 		</tr>   
	 	<%}%>	 
	 </table>
	</div>
</body>
</html>