<%@page import="com.hwua.po.Orders"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
</head>
<body>

</body><head>
<meta charset="utf-8">
<title>Insert title here</title>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
<script src="http://libs.baidu.com/jquery/1.9.1/jquery.js"></script>  
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>   
    <!-- Bootstrap -->
    <link rel="stylesheet" href="/web/css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/web/css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="/web/css/owl.carousel.css">
    <link rel="stylesheet" href="/web/style.css">
    <link rel="stylesheet" href="/web/css/responsive.css">
</head>
<style type="text/css">
 #boxs{
 	margin:0 auto;
 	width: 700px;
 	height: 1200px;
 }
 #boxs th,tr,td{
 	width:150px;
 	border: 2px solid #1abc9c;
 	text-align: center; 
 	padding: 15px 15px;
 }
 #top{
 	margin:0 auto;
 	width: 700px;
 	height: 70px;
 	text-align: center; 
 }	
</style>
<body>
	<%--查询个人订单信息 --%>
	<div class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="user-menu">
                        <ul>
							<li><a href="UseMessage.jsp"><i class="fa fa-user"></i><%=session.getAttribute("username") == null? "":session.getAttribute("username")%></a></li>
                            <li><a href="#"><i class="fa fa-heart"></i> 我的最爱</a></li>
                            <li><a href="cart.html"><i class="fa fa-user"></i> 我的购物车</a></li>
                            <li><a href="checkout.html"><i class="fa fa-user"></i> 结账</a></li>
                            <li><a href="Login.jsp"><i class="fa fa-user"></i> 登录/重新登录</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="header-right">
                        <ul class="list-unstyled list-inline">
                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">currency :</span><span class="value">USD </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">USD</a></li>
                                    <li><a href="#">INR</a></li>
                                    <li><a href="#">GBP</a></li>
                                </ul>
                            </li>

                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">language :</span><span class="value">English </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">English</a></li>
                                    <li><a href="#">French</a></li>
                                    <li><a href="#">German</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End header area -->
    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="logo">
                        <h1><a href="index.html">Vast<span>ElectronicsStore</span></a></h1>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="shopping-item">
                        <a href="cart.html">Cart - <span class="cart-amunt">$800</span> <i class="fa fa-shopping-cart"></i> <span class="product-count">5</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End site branding area -->

<div id="top">
	<h2 style="color: #1abc9c">我的订单信息</h2>
</div>    
<div id="boxs">
 <table cellpadding="0" cellspacing="0">
 	<tr>
 		<th>订单编号</th>
 		<th>创建时间</th>
 		<th>订单状态</th>
 		<th>商品编号</th>
 		<th>收货地址</th>
 		<th>我的账号</th>
 		<th>用户操作</th>
 	</tr>
 	<%List<Orders> list = (List<Orders>)session.getAttribute("userorder"); %>
 	<%for(int i = 0;i<list.size();i++){ %>
 	<tr>
 		<td><%=list.get(i).getOid() %></td>
 		<td><%=list.get(i).getOdate() %></td>
 		<td><%=list.get(i).getOboolean() %></td>
 		<td><%=list.get(i).getPid() %></td>
 		<td><%=list.get(i).getSiteid() %></td>
 		<td><%=list.get(i).getUserid() %></td>
 		<td><a href="DoUserReceipt?oid=<%=list.get(i).getOid()%>">确认收货</a></td>
 	</tr>   
 	<%}%>	 
 </table>
</div>
</body>
</html>