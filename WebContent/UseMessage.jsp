<%@page import="com.hwua.po.Use"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>用户个人信息</title>
</head>
<style>
	#boxs{
		
		width: 300px;
		margin: 0 auto;
		text-align: center;
	}
	h1 {
		text-align: center;
		color: #1abc9c;
	}
	#boxs a{
		font-size: 21px;
		font-family: DFKai-SB;
		
	}
	#boxs input{
		width: 200px;
		height: 25px;
		border: 2px solid gray;
		border-radius: 10px;
	}
	#tops{
		width: 500px;
		height: 20px; 
		margin: 0 auto;
		text-align: center;
	}
	#top{
		width: 500px;
		height: 20px; 
		margin: 0 auto;
		text-align: center;
	}
</style>
<body>
	<h1>我的信息</h1>
	<div id="top">
		<a style="color: red; font-family: LiSu; font-size: 20px;"><%=request.getAttribute("UpdatePhotoSuccess") == null? "":request.getAttribute("UpdatePhotoSuccess") %></a>
	</div>
	<div id="tops">
		<a style="color: red; font-family: LiSu; font-size: 20px;"><%=request.getAttribute("UpdateSuccess") == null? "":request.getAttribute("UpdateSuccess") %></a>
	</div>
<div id="boxs">
	<%Use use = (Use)session.getAttribute("usemessage"); %>
	
	<a></a><img style="width: 100px;height: 100px;" src=<%="/web/img/" + use.getPhoto() %>><br>
	
	<%-- 提交至上传至数据库的servlet --%>
	<form action="UpdateUserPhoto" method="post" enctype="multipart/form-data">
		<input type="file" name="photo">
		<input type="submit" value="上传">
	</form>
	
	<%--提交至修改用户信息的servlet --%>
	<form action="UpdateUserMassage" method="post" >
		
		<br>
		<a>昵称:</a><input type=text name="name" value="<%=use.getName()%>"><br>
		<br>
		<a>简述:</a><input type=text name="text" value="<%=use.getText()%>"><br>
		<br>
		<a>年龄:</a><input type=text name="age" value="<%=use.getAge()%>"><br>
		<br>
		<a>性别:</a><input type=text name="sex" value="<%=use.getSex()%>"><br>
		<br>
		<a>地址:</a><input type=text name="location" value="<%=use.getLocation()%>"><br>
		<br>
		<a>积分:</a><%=use.getUsercode()%><br>
		<br>
		<input type="submit" value="修改">
		<a href="Login.jsp"><input type="button" value="重新登录"  style="height:25px;margin-top: 4px;"></a>
	</form>
</div>
</body>
</html>