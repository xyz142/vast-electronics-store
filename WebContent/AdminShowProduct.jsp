<%@page import="com.hwua.po.Product"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>AdminCRUDProduct</title>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<style type="text/css">
#boxs{
 	margin:0 auto;
 	width: 1500px;
 	height: 1200px;
 	
 }
 #boxs th,tr,td{
 	width:300px;
 	border: 1px solid #1abc9c;
 	text-align: center; 
 	padding: 10px 10px;
 	width: 200px;
 	height: 10px;
 }
 #tops{
 	margin:0 auto;
 	width: 700px;
 	height: 70px;
 	text-align: center; 
 }	
</style>
<body>
	<div id="tops"><h2 style="color: gray;">管理商品</h2></div>
	<div id="boxs">
		<h3><a href="AdminAddProduct.jsp" >添加商品</a></h3>
		<table cellspacing="0" cellpadding="0" >
			<tr>
				<th>商品编号</th>
				<th>商品名</th>
				<th>数量</th>
				<th>现价</th>
				<th>原价</th>
				<th>描述</th>
				<th>上架人</th>
				<th>上架日期</th>
				<th>图片地址</th>
				<th>是否上架</th>
				<th>发货地址</th>
				<th>分类id</th>
				<th>促销</th>
				<th>删除</th>
				<th>修改</th>
			</tr>
			<%
				List<Product> list = (List<Product>)request.getAttribute("ProductList");
			%>
			<%
				//处理数据
				for(int i = 0;i<list.size();i++){
			%>
			<tr>
				<td><%=list.get(i).getPid()%></td>
				<td><%=list.get(i).getPname() %></td>
				<td><%=list.get(i).getPcount() %></td>
				<td><%=list.get(i).getPrice() %></td>
				<td><%=list.get(i).getOldprice() %></td>
				<td><%=list.get(i).getPtext()%></td>
				<td><%=list.get(i).getUids()%></td>
				<td><%=list.get(i).getPdate() %></td>
				<td><%=list.get(i).getPhoto()%></td>
				<td><%=list.get(i).getPboolean() %></td>
				<td><%=list.get(i).getLocations() %></td>
				<td><%=list.get(i).getClazzid() %></td>
				<td><%=list.get(i).getPromotion() %></td>
				<td><a href="AdminDoDeleteAction?pid=<%= list.get(i).getPid() %>">删除</a></td>
				<td><a href="AdminDoUpdateAction?pid=<%= list.get(i).getPid() %>">修改</a></td>
			</tr>
			<%} %>
		</table>
	</div>
</body>
</html>