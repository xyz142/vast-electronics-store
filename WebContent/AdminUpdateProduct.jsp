<%@page import="com.hwua.po.Product"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>AdminUpdateProduct</title>
</head>
<style type="text/css">
	#boxs{
		margin: 0 auto;
		text-align: center
	}
	#boxs a{
		font-size: 20px;
		font-family: LiSu;
		
	}
	#boxs input{
		height: 25px;
		width: 200px;
		border: 2px solid gray;
		border-radius: 10px;
	}
</style>
<body>
<div id="boxs">
	<%
		Product p = (Product)request.getAttribute("productObj");
	%>
	<h2 style="color: gray;">修改商品信息</h2>
	<form action="AdminUpdateAction" method="post">
		<a>商品编号：</a><input type="text" name="pid" value="<%=p.getPid()%>"><br>
		<br>
		<a>商品名称：</a><input type="text" name="pname" value="<%=p.getPname()%>"><br>
		<br>
		<a>商品库存：</a><input type="text" name="pcount" value="<%=p.getPcount()%>"><br>
		<br>
		<a>商品现价：</a><input type="text" name="price" value="<%=p.getPrice()%>"><br>
		<br>
		<a>商品原价：</a><input type="text" name="oldprice" value="<%=p.getOldprice()%>"><br>
		<br>
		<a>商品简述：</a><input type="text" name="ptext" value="<%=p.getPtext()%>"><br>
		<br>
		<a>发布人员：</a><input type="text" name="uid" value="<%=p.getUids()%>"><br>
		<br>
		<a>上架时间：</a><input type="date" name="pdate" value="<%=p.getPdate()%>"><br>
		<br>
		<a>预览照片：</a><input type="text" name="photo" value="<%=p.getPhoto()%>"><br>
		<br>
		<a>是否上架：</a><input type="text" name="pboolean" value="<%=p.getPboolean()%>"><br>
		<br>
		<a>发货坐标：</a><input type="text" name="locations" value="<%=p.getLocations()%>"><br>
		<br>
		<a>分类编号：</a><input type="text" name="clazzid" value="<%=p.getClazzid()%>"><br>
		<br>
		<a>是否促销：</a><input type="text" name="promotion" value="<%=p.getPromotion()%>"><br>
		<br>
		<a>预览照片：</a><input type="text" name="photodetails" value="<%=p.getPhotoDetails()%>"><br>
		<br>
		<a>描述照片：</a><input type="text" name="textdetails" value="<%=p.getTextDetails()%>"><br>
		<br>
		<input type="submit" value="修改">
	</form>
</div>	
</body>
</html>