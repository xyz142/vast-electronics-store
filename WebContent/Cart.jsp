<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="http://libs.baidu.com/jquery/1.9.1/jquery.js"></script>
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>   
    <!-- Bootstrap -->
    <link rel="stylesheet" href="/web/css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/web/css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="/web/css/owl.carousel.css">
    <link rel="stylesheet" href="/web/style.css">
    <link rel="stylesheet" href="/web/css/responsive.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <style type="text/css">
	#boxs{
		margin:0 auto;
		width:790px;
		
		height: 600px;
		border-radius: 10px;
	}
	#boxs img{
		width: 150px;
		height: 100px;
	}
	#boxs tr td{
		padding:15px 15px;
		border:4px solid #1abc9c;
		
	}
	#boxs table{
		margin: 0 auto;
		text-align: center;
		border-radius: 10px;
	}
	#boxs th{
		color: #1abc9c;
	}
	#top{
		margin: 0 auto;
		height: 50px;
		width: 790px; 
	}
</style>
  <body>
    <div class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="user-menu">
                        <ul>
                            <li><a href=""><i class="fa fa-user"></i><%=session.getAttribute("username") == null? "":session.getAttribute("username")%></a></li>
                            <li><a href="#"><i class="fa fa-heart"></i> 我的最爱</a></li>
                            <li><a href="cart.html"><i class="fa fa-user"></i> 我的购物车</a></li>
                            <li><a href="checkout.html"><i class="fa fa-user"></i> 结账</a></li>
                            <li><a href="#"><i class="fa fa-user"></i> 登录</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="header-right">
                        <ul class="list-unstyled list-inline">
                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">currency :</span><span class="value">USD </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">USD</a></li>
                                    <li><a href="#">INR</a></li>
                                    <li><a href="#">GBP</a></li>
                                </ul>
                            </li>

                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">language :</span><span class="value">English </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">English</a></li>
                                    <li><a href="#">French</a></li>
                                    <li><a href="#">German</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End header area -->
    
    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="logo">
                        <h1><a href="index.html">Vast<span>ElectronicsStore</span></a></h1>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="shopping-item">
                        <a href="cart.html">Cart - <span class="cart-amunt">$800</span> <i class="fa fa-shopping-cart"></i> <span class="product-count">5</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div class="mainmenu-area">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div> 
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="First.jsp">首页</a></li>
                        <li><a href="Shop.jsp">商城</a></li>
                        
                        <li class="active"><a href="Cart.jsp">购物车</a></li>
                       
                        <%-- 	
						<li><a href="#">Category</a></li>
						<li><a href="#">Others</a></li>
						<li><a href="#">Contact</a></li>
						--%>	
                    </ul>
                </div>  
            </div>
        </div>
    </div> <!-- End mainmenu area -->
    
    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Shopping Cart</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End Page title area -->  
    
    <div id="top">
    
    </div>
	<div id="boxs">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<th>选择
					<input id="checkall" type="checkbox" name="">
				</th>
				<th>商品编号</th>
				<th>商品图片</th>
				<th>商品名称</th>
				<th>商品价格</th>
				<th>发货地</th>
				<th>购买数量</th>
				<th>操作</th>
			</tr>
			<tbody id="box">
				<tr>
					<td><input type="checkbox" name=""></td>
					<td>----</td>
					<td><img src="#"></td>
					<td>-----</td>
					<td>-----</td>
					<td>-----</td>
					<td><button>-</button><span>-----</span><button>+</button></td>
					<td>删除</td>
				</tr>
			</tbody>
		</table>
		<button id="sub">支付并提交订单</button>
		
	</div>
	<%="共要支付:" + request.getAttribute("count") %>
    <div class="footer-top-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="footer-about-us">
                        <h2>Vast<span>ElectronicsStore</span></h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis sunt id doloribus vero quam laborum quas alias dolores blanditiis iusto consequatur, modi aliquid eveniet eligendi iure eaque ipsam iste, pariatur omnis sint! Suscipit, debitis, quisquam. Laborum commodi veritatis magni at?</p>
                        <div class="footer-social">
                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-menu">
                        <h2 class="footer-wid-title">User Navigation </h2>
                        <ul>
                            <li><a href="#">My account</a></li>
                            <li><a href="#">Order history</a></li>
                            <li><a href="#">Wishlist</a></li>
                            <li><a href="#">Vendor contact</a></li>
                            <li><a href="#">Front page</a></li>
                        </ul>                        
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-menu">
                        <h2 class="footer-wid-title">Categories</h2>
                        <ul>
                            <li><a href="#">Mobile Phone</a></li>
                            <li><a href="#">Home accesseries</a></li>
                            <li><a href="#">LED TV</a></li>
                            <li><a href="#">Computer</a></li>
                            <li><a href="#">Gadets</a></li>
                        </ul>                        
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="footer-newsletter">
                        <h2 class="footer-wid-title">Newsletter</h2>
                        <p>Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>
                        <div class="newsletter-form">
                            <form action="#">
                                <input type="email" placeholder="Type your email">
                                <input type="submit" value="Subscribe">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End footer top area -->
    
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="copyright">
                        <p>Copyright &copy; 2016.Company name All rights reserved.<a target="_blank" href="http://www.mycodes.net/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a></p>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="footer-card-icon">
                        <i class="fa fa-cc-discover"></i>
                        <i class="fa fa-cc-mastercard"></i>
                        <i class="fa fa-cc-paypal"></i>
                        <i class="fa fa-cc-visa"></i>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End footer bottom area -->
   
  </body>
  <script type="text/javascript">
  	$.post("CarFindUid",function(data){
  		var list = JSON.parse(data).list;
  		
  		var str = "";
  		for(var i = 0;i<list.length;i++){
  			var c = list[i];
  			str+="<tr>"
  				+"<td ><input name="+c.cid+" class='ch' type=\"checkbox\"></td>"
  				+"<td>"+c.product.pid+"</td>"
  				+"<td><img src=/web/img/" + c.product.photo + "></td>"
  				+"<td>"+c.product.pname+"</td>"
  				+"<td>￥"+c.product.price+"</td>"
  				+"<td>"+c.product.locations+"</td>"
  				+"<td><button>-</button><span>"+c.count+"</span><button>+</button></td>"
  				+"<td class=\"dodelete\" name="+c.cid+">删除</td>"
  				+"</tr>";
  		}
  		$("#box").html(str);
  	})
  	
  	//删除
  	$("#box").on("click",".dodelete",function(){
  		var cid = $(this).attr("name");
  		console.log(cid);
  		
  		var td = $(this);
  		$.post("DoDeleteCart",{"cid":cid},function(data){
  			var r = JSON.parse(data).r;
  			console.log("r=" + r);
  			if(r>0){
  				alert("删除成功！");
  				td.parent().remove();
  			}
  		})
  	})
  	
  	//全选，反选
  	$("#checkall").click(function(){
  		var thischeck = this.checked;
  		console.log(thischeck);
  		
  		var count = $("#box .ch").length;
  		for(var i = 0;i<count;i++){
  			var check = $("#box .ch")[i];
  			check.checked = thischeck;
  		}
  	})
  	
  	//提交订单
  	$("#sub").click(function(){
  		var orderlist = "";
  		var count = $("#box .ch").length;
  		console.log("提交数目" + count);
  		
  		for(var i = 0;i<count;i++){
  			var check = $("#box .ch")[i];
  			console.log(check.checked);
  			if(check.checked){
  				var cid = check.name;
  				if(orderlist==""){
  					orderlist+=cid;
  				}else{
  					orderlist+=","+cid;
  				}
  			}
  		}
  		console.log(orderlist);
  		//将选中的订单，发送至后台
  		window.location.href="DoSendOrder?orderlist="+orderlist;
  	})
  </script>
</html>