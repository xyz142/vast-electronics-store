<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>首页</title>

<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>

<!-- Bootstrap -->
<link rel="stylesheet" href="/web/css/bootstrap.min.css">

<!-- Font Awesome -->
<link rel="stylesheet" href="/web/css/font-awesome.min.css">

<!-- Custom CSS -->
<link rel="stylesheet" href="/web/css/owl.carousel.css">
<link rel="stylesheet" href="/web/style.css">
<link rel="stylesheet" href="/web/css/responsive.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>

<div class="header-area">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="user-menu">
					<ul>
						<li><a href="UseMessage.jsp"><i class="fa fa-user"></i><%=session.getAttribute("username") == null? "":session.getAttribute("username")%></a></li>
						<li><a href="#"><i class="fa fa-heart"></i> 我的最爱</a></li>
						<li><a href="cart.html"><i class="fa fa-user"></i> 我的购物车</a></li>
						<li><a href="checkout.html"><i class="fa fa-user"></i> 结账</a></li>
						<li><a href="Login.jsp"><i class="fa fa-user"></i>登录/重新登录</a></li>
					</ul>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="header-right">
					<ul class="list-unstyled list-inline">
						<li class="dropdown dropdown-small">
							<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">currency :</span><span class="value">USD </span><b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="#">USD</a></li>
								<li><a href="#">INR</a></li>
								<li><a href="#">GBP</a></li>
							</ul>
						</li>

						<li class="dropdown dropdown-small">
							<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">language :</span><span class="value">English </span><b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="#">English</a></li>
								<li><a href="#">French</a></li>
								<li><a href="#">German</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div> <!-- End header area -->

<div class="site-branding-area">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="logo">
					<h1><a href="index.html">Vast<span>ElectronicsStore </span></a></h1>
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="shopping-item">
					<a href="CheckMyOrders">我的订单 <span class="cart-amunt"></span> <i class="fa fa-shopping-cart"></i> <span class="product-count">Vast</span></a>
				</div>
			</div>
		</div>
	</div>
</div> <!-- End site branding area -->

<div class="mainmenu-area">
	<div class="container">
		<div class="row">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div> 
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="index.html">首页</a></li>
					<li><a href="Shop.jsp">商城</a></li>
					
					<li><a href="Cart.jsp">购物车</a></li>
					
				<%-- 	
					<li><a href="#">Category</a></li>
					<li><a href="#">Others</a></li>
					<li><a href="#">Contact</a></li>
				--%>	
				</ul>
			</div>  
		</div>
	</div>
</div> <!-- End mainmenu area -->

<div class="slider-area">
	<div class="zigzag-bottom"></div>
	<div id="slide-list" class="carousel carousel-fade slide" data-ride="carousel">
		<div class="slide-bulletz">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ol class="carousel-indicators slide-indicators">
							<li data-target="#slide-list" data-slide-to="0" class="active"></li>
							<li data-target="#slide-list" data-slide-to="1"></li>
							<li data-target="#slide-list" data-slide-to="2"></li>
						</ol>                            
					</div>
				</div>
			</div>
		</div>

		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="single-slide">
					<div class="slide-bg slide-one"></div>
					<div class="slide-text-wrapper">
						<div class="slide-text">
							<div class="container">
								<div class="row">
									<div class="col-md-6 col-md-offset-6">
										<div class="slide-content">
											<h2>VastStore 新品首发</h2>
											<p>下单立减 199-1999元</p>
											
											<a href="" class="readmore">现在购买</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="single-slide">
					<div class="slide-bg slide-two"></div>
					<div class="slide-text-wrapper">
						<div class="slide-text">
							<div class="container">
								<div class="row">
									<div class="col-md-6 col-md-offset-6">
										<div class="slide-content">
											<h2>VastStore 新品首发</h2>
											<p>下单立减 199-1999元</p>
											<a href="" class="readmore">现在购买</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="single-slide">
					<div class="slide-bg slide-three"></div>
					<div class="slide-text-wrapper">
						<div class="slide-text">
							<div class="container">
								<div class="row">
									<div class="col-md-6 col-md-offset-6">
										<div class="slide-content">
											<h2>VastStore 新品首发</h2>
											<p>下单立减 199-1999元</p>
											<a href="" class="readmore">现在购买</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>        
</div> <!-- End slider area -->

<div class="promo-area">
	<div class="zigzag-bottom"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6">
				<div class="single-promo">
					<i class="fa fa-refresh"></i>
					<p>30日免费退换</p>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="single-promo">
					<i class="fa fa-truck"></i>
					<p>各地区包邮</p>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="single-promo">
					<i class="fa fa-lock"></i>
					<p>安全的支付环境</p>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="single-promo">
					<i class="fa fa-gift"></i>
					<p>购买即送赠品</p>
				</div>
			</div>
		</div>
	</div>
</div> <!-- End promo area -->

<div class="maincontent-area">
	<div class="zigzag-bottom"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="latest-product">
					<h2 class="section-title">部分产品展示</h2>
					<div class="product-carousel">
						<div class="single-product">
							<div class="product-f-image">
								<img src="/web/img/lenovo2.jpg" alt="">
								<div class="product-hover">
									<a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> 特别优惠</a>
									<a href="http://localhost:8080/myWebProject/ShowDetails?pid=107" class="view-details-link"><i class="fa fa-link"></i> 查看详情</a>
								</div>
							</div>
							
							<h2><a href="single-product.html">Lenovo LEGION Y7000P</a></h2>
							
							<div class="product-carousel-price">
								<ins>￥5399</ins> <del>￥5799</del>
							</div> 
						</div>
						<div class="single-product">
							<div class="product-f-image">
								<img src="/web/img/ps5.jpg" alt="">
								<div class="product-hover">
									<a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> 特别优惠</a>
									<a href="http://localhost:8080/myWebProject/ShowDetails?pid=105" class="view-details-link"><i class="fa fa-link"></i> 查看详情</a>
								</div>
							</div>
							
							<h2><a href="single-product.html">PlayStation 5</a></h2>
							<div class="product-carousel-price">
								<ins>￥4599</ins> <del>￥4999</del>
							</div> 
						</div>
						<div class="single-product">
							<div class="product-f-image">
								<img src="/web/img/zhuowei2.jpg" alt="">
								<div class="product-hover">
									<a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> 特别优惠</a>
									<a href="http://localhost:8080/myWebProject/ShowDetails?pid=112" class="view-details-link"><i class="fa fa-link"></i> 查看详情</a>
								</div>
							</div>
							
							<h2><a href="single-product.html">ZOWIE GEAR 显示器</a></h2>

							<div class="product-carousel-price">
								<ins>￥3999</ins> <del>￥4199</del>
							</div>                                 
						</div>
						<div class="single-product">
							<div class="product-f-image">
								<img src="/web/img/rog.jpg" alt="">
								<div class="product-hover">
									<a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i>特别优惠</a>
									<a href="http://localhost:8080/myWebProject/ShowDetails?pid=109" class="view-details-link"><i class="fa fa-link"></i> 查看详情</a>
								</div>
							</div>
							
							<h2><a href="single-product.html">ASUS ROG 幻14</a></h2>

							<div class="product-carousel-price">
								<ins>￥13999</ins> <del>￥14999</del>
							</div>                            
						</div>
						<div class="single-product">
							<div class="product-f-image">
								<img src="/web/img/ps4.jpg" alt="">
								<div class="product-hover">
									<a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> 特别优惠</a>
									<a href="http://localhost:8080/myWebProject/ShowDetails?pid=103" class="view-details-link"><i class="fa fa-link"></i> 查看详情</a>
								</div>
							</div>
							
							<h2><a href="single-product.html">PlayStation 4</a></h2>

							<div class="product-carousel-price">
								<ins>￥3299</ins> <del>￥3599</del>
							</div>                                 
						</div>
						<div class="single-product">
							<div class="product-f-image">
								<img src="/web/img/xbox.jpg" alt="">
								<div class="product-hover">
									<a href="#" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i>特别优惠</a>
									<a href="single-product.html" class="view-details-link"><i class="fa fa-link"></i> 查看详情</a>
								</div>
							</div>
							
							<h2><a href="single-product.html">xbox one 游戏手柄</a></h2>

							<div class="product-carousel-price">
								<ins>￥459</ins> <del>￥329</del>
							</div>                            
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> <!-- End main content area -->

<div class="brands-area">
	<div class="zigzag-bottom"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="brand-wrapper">
					<h2 class="section-title">Brands</h2>
					<div class="brand-list">
						<img src="/web/img/services_logo__1.jpg" alt="">
						<img src="/web/img/services_logo__2.jpg" alt="">
						<img src="/web/img/services_logo__3.jpg" alt="">
						<img src="/web/img/services_logo__4.jpg" alt="">
						<img src="/web/img/services_logo__1.jpg" alt="">
						<img src="/web/img/services_logo__2.jpg" alt="">
						<img src="/web/img/services_logo__3.jpg" alt="">
						<img src="/web/img/services_logo__4.jpg" alt="">                            
					</div>
				</div>
			</div>
		</div>
	</div>
</div> <!-- End brands area -->

<div class="product-widget-area">
	<div class="zigzag-bottom"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="single-product-widget">
					<h2 class="product-wid-title">Top Sellers</h2>
					<a href="" class="wid-view-more">View All</a>
					<div class="single-wid-product">
						<a href="single-product.html"><img src="/web/img/product-thumb-1.jpg" alt="" class="product-thumb"></a>
						<h2><a href="single-product.html">Sony Smart TV - 2015</a></h2>
						<div class="product-wid-rating">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
						</div>
						<div class="product-wid-price">
							<ins>$400.00</ins> <del>$425.00</del>
						</div>                            
					</div>
					<div class="single-wid-product">
						<a href="single-product.html"><img src="/web/img/product-thumb-2.jpg" alt="" class="product-thumb"></a>
						<h2><a href="single-product.html">Apple new mac book 2015</a></h2>
						<div class="product-wid-rating">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
						</div>
						<div class="product-wid-price">
							<ins>$400.00</ins> <del>$425.00</del>
						</div>                            
					</div>
					<div class="single-wid-product">
						<a href="single-product.html"><img src="/web/img/product-thumb-3.jpg" alt="" class="product-thumb"></a>
						<h2><a href="single-product.html">Apple new i phone 6</a></h2>
						<div class="product-wid-rating">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
						</div>
						<div class="product-wid-price">
							<ins>$400.00</ins> <del>$425.00</del>
						</div>                            
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="single-product-widget">
					<h2 class="product-wid-title">Recently Viewed</h2>
					<a href="#" class="wid-view-more">View All</a>
					<div class="single-wid-product">
						<a href="single-product.html"><img src="/web/img/product-thumb-4.jpg" alt="" class="product-thumb"></a>
						<h2><a href="single-product.html">Sony playstation microsoft</a></h2>
						<div class="product-wid-rating">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
						</div>
						<div class="product-wid-price">
							<ins>$400.00</ins> <del>$425.00</del>
						</div>                            
					</div>
					<div class="single-wid-product">
						<a href="single-product.html"><img src="/web/img/product-thumb-1.jpg" alt="" class="product-thumb"></a>
						<h2><a href="single-product.html">Sony Smart Air Condtion</a></h2>
						<div class="product-wid-rating">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
						</div>
						<div class="product-wid-price">
							<ins>$400.00</ins> <del>$425.00</del>
						</div>                            
					</div>
					<div class="single-wid-product">
						<a href="single-product.html"><img src="/web/img/product-thumb-2.jpg" alt="" class="product-thumb"></a>
						<h2><a href="single-product.html">Samsung gallaxy note 4</a></h2>
						<div class="product-wid-rating">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
						</div>
						<div class="product-wid-price">
							<ins>$400.00</ins> <del>$425.00</del>
						</div>                            
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="single-product-widget">
					<h2 class="product-wid-title">Top New</h2>
					<a href="#" class="wid-view-more">View All</a>
					<div class="single-wid-product">
						<a href="single-product.html"><img src="/web/img/product-thumb-3.jpg" alt="" class="product-thumb"></a>
						<h2><a href="single-product.html">Apple new i phone 6</a></h2>
						<div class="product-wid-rating">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
						</div>
						<div class="product-wid-price">
							<ins>$400.00</ins> <del>$425.00</del>
						</div>                            
					</div>
					<div class="single-wid-product">
						<a href="single-product.html"><img src="/web/img/product-thumb-4.jpg" alt="" class="product-thumb"></a>
						<h2><a href="single-product.html">Samsung gallaxy note 4</a></h2>
						<div class="product-wid-rating">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
						</div>
						<div class="product-wid-price">
							<ins>$400.00</ins> <del>$425.00</del>
						</div>                            
					</div>
					<div class="single-wid-product">
						<a href="single-product.html"><img src="/web/img/product-thumb-1.jpg" alt="" class="product-thumb"></a>
						<h2><a href="single-product.html">Sony playstation microsoft</a></h2>
						<div class="product-wid-rating">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
						</div>
						<div class="product-wid-price">
							<ins>$400.00</ins> <del>$425.00</del>
						</div>                            
					</div>
				</div>
			</div>
		</div>
	</div>
</div> <!-- End product widget area -->

<div class="footer-top-area">
	<div class="zigzag-bottom"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6">
				<div class="footer-about-us">
					<h2>e<span>Electronics</span></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis sunt id doloribus vero quam laborum quas alias dolores blanditiis iusto consequatur, modi aliquid eveniet eligendi iure eaque ipsam iste, pariatur omnis sint! Suscipit, debitis, quisquam. Laborum commodi veritatis magni at?</p>
					<div class="footer-social">
						<a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
						<a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
						<a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
						<a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
						<a href="#" target="_blank"><i class="fa fa-pinterest"></i></a>
					</div>
				</div>
			</div>
			
			<div class="col-md-3 col-sm-6">
				<div class="footer-menu">
					<h2 class="footer-wid-title">User Navigation </h2>
					<ul>
						<li><a href="#">My account</a></li>
						<li><a href="#">Order history</a></li>
						<li><a href="#">Wishlist</a></li>
						<li><a href="#">Vendor contact</a></li>
						<li><a href="#">Front page</a></li>
					</ul>                        
				</div>
			</div>
			
			<div class="col-md-3 col-sm-6">
				<div class="footer-menu">
					<h2 class="footer-wid-title">Categories</h2>
					<ul>
						<li><a href="#">Mobile Phone</a></li>
						<li><a href="#">Home accesseries</a></li>
						<li><a href="#">LED TV</a></li>
						<li><a href="#">Computer</a></li>
						<li><a href="#">Gadets</a></li>
					</ul>                        
				</div>
			</div>
			
			<div class="col-md-3 col-sm-6">
				<div class="footer-newsletter">
					<h2 class="footer-wid-title">Newsletter</h2>
					<p>Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>
					<div class="newsletter-form">
						<form action="#">
							<input type="email" placeholder="Type your email">
							<input type="submit" value="Subscribe">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> <!-- End footer top area -->

<div class="footer-bottom-area">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="copyright">
					<p>Copyright &copy; 2016.Company name All rights reserved.<a target="_blank" href="http://www.mycodes.net/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a></p>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="footer-card-icon">
					<i class="fa fa-cc-discover"></i>
					<i class="fa fa-cc-mastercard"></i>
					<i class="fa fa-cc-paypal"></i>
					<i class="fa fa-cc-visa"></i>
				</div>
			</div>
		</div>
	</div>
</div> <!-- End footer bottom area -->

<!-- Latest jQuery form server -->
<script src="/web/js/jquery-1.8.3.min.js"></script>

<!-- Bootstrap JS form CDN -->
<script src="/web/js/bootstrap.min.js"></script>

<!-- jQuery sticky menu -->
<script src="/web/js/owl.carousel.min.js"></script>
<script src="/web/js/jquery.sticky.js"></script>

<!-- jQuery easing -->
<script src="/web/js/jquery.easing.1.3.min.js"></script>

<!-- Main Script -->
<script src="/web/js/main.js"></script>
</body>
</html>