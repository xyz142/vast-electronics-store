<%@page import="com.hwua.po.Product"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
<script src="http://libs.baidu.com/jquery/1.9.1/jquery.js"></script>  
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>   
    <!-- Bootstrap -->
    <link rel="stylesheet" href="/web/css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/web/css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="/web/css/owl.carousel.css">
    <link rel="stylesheet" href="/web/style.css">
    <link rel="stylesheet" href="/web/css/responsive.css">
</head>
<style>
	div{
		margin: 0 auto;
		text-align: center;
	}
	h1{
		color:#1abc9c;
	}
	h2{
		color:pink;
	}
	#boxs .show {
    position: relative;
    width: 100%;
    display: block;
    margin: 0 auto;
    left:135px;
    top:75px;
    
}
.show tbody tr {
    display: inline-block;
   
}
.show th, td {
    height: 300px;
    display: block;
  
}	
#fy table{
	margin: 0 auto;
	text-align: center;
	bottom: 500px;
}
</style>
<body>
	<%--模糊查询 --%>
	<div class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="user-menu">
                        <ul>
                            <li><a href="#"><i class="fa fa-user"></i> 我的信息</a></li>
                            <li><a href="#"><i class="fa fa-heart"></i> 我的最爱</a></li>
                            <li><a href="cart.html"><i class="fa fa-user"></i> 我的购物车</a></li>
                            <li><a href="checkout.html"><i class="fa fa-user"></i> 结账</a></li>
                            <li><a href="#"><i class="fa fa-user"></i> 登录</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="header-right">
                        <ul class="list-unstyled list-inline">
                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">currency :</span><span class="value">USD </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">USD</a></li>
                                    <li><a href="#">INR</a></li>
                                    <li><a href="#">GBP</a></li>
                                </ul>
                            </li>

                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">language :</span><span class="value">English </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">English</a></li>
                                    <li><a href="#">French</a></li>
                                    <li><a href="#">German</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End header area -->
    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="logo">
                        <h1><a href="index.html">Vast<span>ElectronicsStore</span></a></h1>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="shopping-item">
                        <a href="cart.html">Cart - <span class="cart-amunt">$800</span> <i class="fa fa-shopping-cart"></i> <span class="product-count">5</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End site branding area -->
    
    <div id="cx"><h1>检索到的数据</h1></div>
<div id="boxs">
 <table class="show">
    <tbody>
    
    <%List<Product> list = (List<Product>)request.getAttribute("selectlist"); %>
     <%
   		for(int i = 0;i<list.size();i++){%>
   		
   		<tr>
   	 		<td><img style="width: 280px; height: 300px;" src=<%="/web/img/" + list.get(i).getPhoto() %>></td>
   	 	
   	 		<td><a style="font-size: 20px;font-weight: 700;" href="ShowDetails?pid=<%=list.get(i).getPid()%>"><%=list.get(i).getPname() %></a></td>
   	 	</tr>
   	 	<tr>	
   	 		<td><a style="font-weight: 600;"><%="秒杀价:" + list.get(i).getPrice()  + "￥"%></a><del style="font-weight: 600"><%=list.get(i).getOldprice() + "￥" %></del></td>
   	 	
           	<td><div class="product-option-shop">
               <a class="add_to_cart_button" data-quantity="1" data-product_sku="" data-product_id="70" rel="nofollow" href="/canvas/shop/?add-to-cart=70">Add to cart</a>
            </div></td>                                    
   	 	</tr>
   	 	<%}%>
   		</tbody>
   		<%--<a style="font-weight: 600"><button id="front">上一页</button></a><a style="font-weight: 600">共<span id="pageCount"></span>页 </a><a style="font-weight: 600"><button id="next">下一页</button></a> --%>  
  </table>
</div>
</body>
</html>