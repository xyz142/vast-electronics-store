package com.hwua.po;
/*
 * ���û���
 * 
 */
public class Use {
	private int Num;
	private String Password;
	private String Name;
	private String isRoot;
	private String Text;
	private String Photo;
	private int Age;
	private String Sex;
	private String Location;
	private int Usercode;
	private String rootlevel;
	
	public Use() {
		super();
	}
	
	public String getRootlevel() {
		return rootlevel;
	}

	public void setRootlevel(String rootlevel) {
		this.rootlevel = rootlevel;
	}

	public Use(int num, String password, String name, String isRoot) {
		super();
		Num = num;
		Password = password;
		Name = name;
		this.isRoot = isRoot;
	}

	public int getNum() {
		return Num;
	}

	public void setNum(int num) {
		Num = num;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getIsRoot() {
		return isRoot;
	}

	public void setIsRoot(String isRoot) {
		this.isRoot = isRoot;
	}
	
	public String getText() {
		return Text;
	}

	public void setText(String text) {
		Text = text;
	}

	public String getPhoto() {
		return Photo;
	}

	public void setPhoto(String photo) {
		Photo = photo;
	}

	public int getAge() {
		return Age;
	}

	public void setAge(int age) {
		Age = age;
	}

	public String getSex() {
		return Sex;
	}

	public void setSex(String sex) {
		Sex = sex;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public int getUsercode() {
		return Usercode;
	}

	public void setUsercode(int usercode) {
		Usercode = usercode;
	}

	@Override
	public String toString() {
		return "Use [Num=" + Num + ", Password=" + Password + ", Name=" + Name + ", isRoot=" + isRoot + ", Text=" + Text
				+ ", Photo=" + Photo + ", Age=" + Age + ", Sex=" + Sex + ", Location=" + Location + ", Usercode="
				+ Usercode + ", rootlevel=" + rootlevel + "]";
	}
}
