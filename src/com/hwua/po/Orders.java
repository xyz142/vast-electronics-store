package com.hwua.po;
/*
 * ����
 * 
 */
public class Orders {
	private int oid;
	private String odate;
	private String oboolean;
	private int Pid;
	private String siteid;
	private int userid;
	
	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public String getOdate() {
		return odate;
	}
	public void setOdate(String odate) {
		this.odate = odate;
	}
	public String getOboolean() {
		return oboolean;
	}
	public void setOboolean(String oboolean) {
		this.oboolean = oboolean;
	}
	public int getPid() {
		return Pid;
	}
	public void setPid(int pid) {
		Pid = pid;
	}
	public String getSiteid() {
		return siteid;
	}
	public void setSiteid(String siteid) {
		this.siteid = siteid;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	
	@Override
	public String toString() {
		return "Orders [oid=" + oid + ", odate=" + odate + ", oboolean=" + oboolean + ", Pid=" + Pid + ", siteid="
				+ siteid + ", userid=" + userid + "]";
	}
	
}
