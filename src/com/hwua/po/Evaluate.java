package com.hwua.po;
/*
 * ����
 * 
 */
public class Evaluate {
	private int eid;
	private int userid;
	private String username;
	private String etext;
	private String ecode;
	private int Pid;
	
	public int getEid() {
		return eid;
	}
	public void setEid(int eid) {
		this.eid = eid;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEtext() {
		return etext;
	}
	public void setEtext(String etext) {
		this.etext = etext;
	}
	public String getEcode() {
		return ecode;
	}
	public void setEcode(String ecode) {
		this.ecode = ecode;
	}
	public int getPid() {
		return Pid;
	}
	public void setPid(int pid) {
		Pid = pid;
	}
	
	@Override
	public String toString() {
		return "Evaluate [eid=" + eid + ", userid=" + userid + ", username=" + username + ", etext=" + etext
				+ ", ecode=" + ecode + ", Pid=" + Pid + "]";
	}
}
