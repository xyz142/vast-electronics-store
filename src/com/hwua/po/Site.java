package com.hwua.po;
/*
 * �ջ���ַpo
 * 
 */
public class Site {
	private int sid;
	private int userid;
	private String location;
	private String Locationtext;
	
	public int getSid() {
		return sid;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getLocationtext() {
		return Locationtext;
	}
	public void setLocationtext(String locationtext) {
		Locationtext = locationtext;
	}
	
	@Override
	public String toString() {
		return "Site [sid=" + sid + ", userid=" + userid + ", location=" + location + ", Locationtext=" + Locationtext
				+ "]";
	}
}
