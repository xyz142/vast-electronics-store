package com.hwua.po;
/*
 * 商品类po
 * 
 */
public class Product {
	private int Pid;
	private String pname;
	private int pcount;
	private double Price;
	private double oldprice;
	private String ptext;
	private int Uids;
	private String pdate;
	private String photo;
	private String pboolean;
	private String Locations;
	private int Clazzid;//分类id
	private String promotion;//促销
	private String photoDetails;
	private String textDetails;
	
	
	public String getPhotoDetails() {
		return photoDetails;
	}
	public void setPhotoDetails(String photoDetails) {
		this.photoDetails = photoDetails;
	}
	public String getTextDetails() {
		return textDetails;
	}
	public void setTextDetails(String textDetails) {
		this.textDetails = textDetails;
	}
	public int getPid() {
		return Pid;
	}
	public void setPid(int pid) {
		Pid = pid;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public int getPcount() {
		return pcount;
	}
	public void setPcount(int pcount) {
		this.pcount = pcount;
	}
	public double getPrice() {
		return Price;
	}
	public void setPrice(double price) {
		Price = price;
	}
	public double getOldprice() {
		return oldprice;
	}
	public void setOldprice(double oldprice) {
		this.oldprice = oldprice;
	}
	public String getPtext() {
		return ptext;
	}
	public void setPtext(String ptext) {
		this.ptext = ptext;
	}
	public int getUids() {
		return Uids;
	}
	public void setUids(int uids) {
		Uids = uids;
	}
	public String getPdate() {
		return pdate;
	}
	public void setPdate(String pdate) {
		this.pdate = pdate;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getPboolean() {
		return pboolean;
	}
	public void setPboolean(String pboolean) {
		this.pboolean = pboolean;
	}
	public String getLocations() {
		return Locations;
	}
	public void setLocations(String locations) {
		Locations = locations;
	}
	public int getClazzid() {
		return Clazzid;
	}
	public void setClazzid(int clazzid) {
		Clazzid = clazzid;
	}
	public String getPromotion() {
		return promotion;
	}
	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}
	
	@Override
	public String toString() {
		return "Product [Pid=" + Pid + ", pname=" + pname + ", pcount=" + pcount + ", Price=" + Price + ", oldprice="
				+ oldprice + ", ptext=" + ptext + ", Uids=" + Uids + ", pdate=" + pdate + ", photo=" + photo
				+ ", pboolean=" + pboolean + ", Locations=" + Locations + ", Clazzid=" + Clazzid + ", promotion="
				+ promotion + ", photoDetails=" + photoDetails + ", textDetails=" + textDetails + "]";
	}
	
}
