package com.hwua.po;
/*
 * ���ﳵ
 * 
 */
public class Car {
	private int Cid;
	private int Pid;
	private int userid;
	private int count;
	private Product product;
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getCid() {
		return Cid;
	}
	public void setCid(int cid) {
		Cid = cid;
	}
	public int getPid() {
		return Pid;
	}
	public void setPid(int pid) {
		Pid = pid;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	@Override
	public String toString() {
		return "Car [Cid=" + Cid + ", Pid=" + Pid + ", userid=" + userid + ", count=" + count + ", product=" + product
				+ "]";
	}
	
}
