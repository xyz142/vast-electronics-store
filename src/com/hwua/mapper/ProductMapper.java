package com.hwua.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hwua.po.Product;
import com.hwua.po.Use;

/*
 * ��Ʒӳ��
 * 
 */
//Pid pname pcount Price oldprice ptext Uid pdate photo pboolean Location Clazzid promotion


public class ProductMapper implements IMapper {
	public List<Product> Mapper(ResultSet rs) {
		List<Product>list = new ArrayList<Product>();
		try {
			while(rs.next()) {
				Product p = new Product();
				p.setPid(rs.getInt(1));
				p.setPname(rs.getString(2));
				p.setPcount(rs.getInt(3));
				p.setPrice(rs.getDouble(4));
				p.setOldprice(rs.getDouble(5));
				p.setPtext(rs.getString(6));
				p.setUids(rs.getInt(7));
				p.setPdate(rs.getString(8));
				p.setPhoto(rs.getString(9));
				p.setPboolean(rs.getString(10));
				p.setLocations(rs.getString(11));
				p.setClazzid(rs.getInt(12));
				p.setPromotion(rs.getString(13));
				p.setPhotoDetails(rs.getString(14));
				p.setTextDetails(rs.getString(15));
				
				list.add(p);
				
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return list;
	}
}
