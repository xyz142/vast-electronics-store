package com.hwua.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hwua.po.Site;

public class SiteMapper implements IMapper {
	public List<Site> Mapper(ResultSet rs) {
		List<Site>list = new ArrayList<Site>();
		try {
			while(rs.next()) {
			Site s = new Site();
			s.setSid(rs.getInt(1));
			s.setUserid(rs.getInt(2));
			s.setLocation(rs.getString(3));
			s.setLocationtext(rs.getString(4));
			
			list.add(s);
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return list;
	}
}
