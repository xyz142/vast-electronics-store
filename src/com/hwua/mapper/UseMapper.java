package com.hwua.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hwua.po.Use;
/*
 * �û�ӳ��
 * 
 */
public class UseMapper implements IMapper{

	@Override
	public List<Use> Mapper(ResultSet rs) {
		List<Use>list = new ArrayList<Use>();
		try {
			while(rs.next()) {
				Use u = new Use();
				u.setNum(rs.getInt("NUM"));
				u.setPassword(rs.getString("PASSWORD"));
				u.setName(rs.getString("NAME"));
				u.setIsRoot(rs.getString("ISROOT"));
				u.setText(rs.getString("TEXT"));
				u.setPhoto(rs.getString("PHOTO"));
				u.setAge(rs.getInt("AGE"));
				u.setSex(rs.getString("SEX"));
				u.setLocation(rs.getString("LOCATION"));
				u.setUsercode(rs.getInt("USERCODE"));
				u.setRootlevel(rs.getString("ROOTLEVEL"));
				
				list.add(u);
				
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return list;
	}
}
