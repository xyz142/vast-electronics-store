package com.hwua.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hwua.po.Clazz;

public class ClazzMapper implements IMapper {
	public List<Clazz> Mapper(ResultSet rs) {
		List<Clazz>list = new ArrayList<Clazz>();
		try {
			while(rs.next()) {
				Clazz c = new Clazz();
				c.setCid(rs.getInt(1));
				c.setCname(rs.getString(2));
				
				list.add(c);
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return list;
	}
}
