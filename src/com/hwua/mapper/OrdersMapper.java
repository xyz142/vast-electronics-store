package com.hwua.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hwua.po.Orders;

public class OrdersMapper implements IMapper {
	public List<Orders> Mapper(ResultSet rs) {
		List<Orders>list = new ArrayList<Orders>();
		try {
			while(rs.next()) {
				Orders o = new Orders();
				o.setOid(rs.getInt(1));
				o.setOdate(rs.getString(2));
				o.setOboolean(rs.getString(3));
				o.setPid(rs.getInt(4));
				o.setSiteid(rs.getString(5));
				o.setUserid(rs.getInt(6));
				
				list.add(o);
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return list;
	}
}
