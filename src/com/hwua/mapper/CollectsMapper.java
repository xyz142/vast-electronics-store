package com.hwua.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hwua.po.Collects;

public class CollectsMapper implements IMapper{
	public List<Collects> Mapper(ResultSet rs) {
		List<Collects>list = new ArrayList<Collects>();
		try {
			while(rs.next()) {
			Collects c = new Collects();
			c.setCid(rs.getInt(1));
			c.setPid(rs.getInt(2));
			c.setUserid(rs.getInt(3));
			c.setCount(rs.getInt(4));
			
			list.add(c);
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return list;
	}
}
