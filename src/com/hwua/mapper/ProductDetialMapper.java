package com.hwua.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hwua.po.Productdetial;
import com.hwua.po.Use;

public class ProductDetialMapper implements IMapper {
	public List<Productdetial> Mapper(ResultSet rs) {
		List<Productdetial>list = new ArrayList<Productdetial>();
		try {
			while(rs.next()) {
				Productdetial p = new Productdetial();
				p.setPid(rs.getInt(1));
				p.setPdbtext(rs.getString(2));
				p.setPphotos(rs.getString(3));
				p.setPtextphotos(rs.getString(4));
				
				list.add(p);
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return list;
	}
}
