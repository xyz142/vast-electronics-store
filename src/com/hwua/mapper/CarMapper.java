package com.hwua.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hwua.po.Car;

public class CarMapper implements IMapper {
	public List<Car> Mapper(ResultSet rs) {
		List<Car>list = new ArrayList<Car>();
		try {
			while(rs.next()) {
				Car c = new Car();
				c.setCid(rs.getInt(1));
				c.setPid(rs.getInt(2));
				c.setUserid(rs.getInt(3));
				c.setCount(rs.getInt(4));
				
				list.add(c);
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return list;
	}
}
