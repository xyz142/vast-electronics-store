package com.hwua.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hwua.po.Evaluate;

public class EvaluateMapper implements IMapper{
	public List<Evaluate> Mapper(ResultSet rs) {
		List<Evaluate>list = new ArrayList<Evaluate>();
		try {
			while(rs.next()) {
				Evaluate e = new Evaluate();
				e.setEid(rs.getInt(1));
				e.setUserid(rs.getInt(2));
				e.setUsername(rs.getString(3));
				e.setEtext(rs.getString(4));
				e.setEcode(rs.getString(5));
				e.setPid(rs.getInt(6));
				
				list.add(e);
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return list;
	}
}
