package com.hwua.dao;
/*
 * OrdersSqlMethod
 * 
 */

import java.util.List;

import com.hwua.jdbc.JdbcUtils;
import com.hwua.mapper.CarMapper;
import com.hwua.mapper.OrdersMapper;
import com.hwua.po.Car;
import com.hwua.po.Orders;

public class OrdersDao {
	public static JdbcUtils ju = new JdbcUtils();
	
	//Select
	public List<Orders> Select(){
		String sql = "select oid,to_char(odate,'YYYY-MM-DD HH24:MI:SS')odate,oboolean,pid,siteid,userid from orders";
		Object params[] = {};
		OrdersMapper mapper = new OrdersMapper();
		List<Orders> list = ju.executeQuery(sql, mapper, params);
		return list;
		//System.out.println(list);
	}
	
	//Add
	public int Add(Orders o) {
		int r = -1;
		String sql = "insert into orders(oid,odate,oboolean,pid,siteid,userid) "
					+ "values(SEQ_ORDERS_OID.nextval,to_date(?,'YYYY-MM-DD HH24:MI:SS'),?,?,?,?)";
		Object params[] = {o.getOdate(),o.getOboolean(),o.getPid(),o.getSiteid(),o.getUserid()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//Update
	public int Update(Orders o) {
		int r = -1;
		String sql = "update orders set odate=to_date(?,'yyyy-mm-dd HH:MM:SS'),oboolean=?,pid=?,siteid=?,userid=? "
					+ "where oid=?";
		Object params[] = {o.getOdate(),o.getOboolean(),o.getPid(),o.getSiteid(),o.getUserid(),o.getOid()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//delete
	public int Delete(int oid) {
		int r = -1;
		String sql = "Delete from orders where oid = ?";
		Object params[] = {oid};
		r = ju.executeUpdate(sql, params);
		
		return r;
	}
	
	//select by userid
	public List<Orders> findByUserid(int userid){
		String sql = "select * from orders where userid = ?";
		OrdersMapper mapper = new OrdersMapper();
		Object params[] = {userid};
		
		List<Orders> list = ju.executeQuery(sql, mapper, params);
		return list;
	}
	
	//update order oboolean
	public int UpdateOboolean(int oid) {
		int r = -1;
		String sql = "Update orders set oboolean='已发货' where oid = ?";
		Object params[] = {oid};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//update order user receipt
	public int UpdateReceipt(int oid) {
		int r = -1;
		String sql = "Update orders set oboolean='已收货' where oid = ?";
		Object params[] = {oid};
		r = ju.executeUpdate(sql, params);
		return r;
	}
}
