package com.hwua.dao;
/*
 * ProductSqlMethod
 * 
 */
import java.util.List;

import org.junit.Test;

import com.hwua.jdbc.JdbcUtils;
import com.hwua.mapper.ProductMapper;
import com.hwua.mapper.ProductMapper;
import com.hwua.mapper.ProductMapper;
import com.hwua.mapper.UseMapper;
import com.hwua.po.Product;
import com.hwua.po.Product;
import com.hwua.po.Product;
import com.hwua.po.Use;
public class ProductDao {
	public static JdbcUtils ju = new JdbcUtils();
	
	//select all
	public List<Product> SelectProduct(){
		String sql = "select pid,pname,pcount,price,oldprice,ptext,uids,to_char(pdate,'yyyy-mm-dd HH:MM:SS')pdate,photo,pboolean,locations,clazzid,promotion,photodetails,textdetails from product";
		ProductMapper um = new ProductMapper();
		Object[]params = {};
		List<Product> list = ju.executeQuery(sql, um, params);
		return list;
		//System.out.println(list);
	}
	//page select
	public List<Product> findByPage(int pageSize,int pageNow){
		int star = (pageNow - 1)*pageSize;
		String sql = "";
		sql += "select A.* from (select rownum rn,d.* from Product d) A where rn>? and rownum<=?";
		Object[]params = {star,pageSize};
		ProductMapper dm = new ProductMapper();
		List<Product> list = ju.executeQuery(sql, dm, params);
		return list;
	}
	
	public int getTotal() {
		String sql = "select count(*) from Product";
		int result = ju.executeQueryJvHe(sql, null);
		return result;
	}
	
	//add
	public int Add(Product p) {
		int r = -1;
		String sql = "insert into product (pid,pname,pcount,price,oldprice,ptext,uids,pdate,photo,pboolean,locations,clazzid,promotion,photodetails,textdetails)" 
					 + "values(SEQ_PRODUCT_NUM.nextval,?,?,?,?,?,?,to_date(?,'YYYY-MM-DD HH24:MI:SS'),?,?,?,?,?,?,?)";
		Object[]params = {p.getPname(),p.getPcount(),p.getPrice(),p.getOldprice(),p.getPtext(),p.getUids(),p.getPdate(),p.getPhoto(),p.getPboolean(),p.getLocations(),p.getClazzid(),p.getPromotion(),p.getPhotoDetails(),p.getTextDetails()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//Update
	public int Update(Product p) {
		int r = -1;
		String sql = "update product set pname=?,pcount=?,price=?,oldprice=?,ptext=?,uids=?,pdate=to_date(?,'YYYY-MM-DD HH24:MI:SS'),photo=?,pboolean=?,locations=?,clazzid=?,promotion=?,photodetails=?,textdetails=?"
					+ " where pid = ?";
		Object[]params = {p.getPname(),p.getPcount(),p.getPrice(),p.getOldprice(),p.getPtext(),p.getUids(),p.getPdate(),p.getPhoto(),p.getPboolean(),p.getLocations(),p.getClazzid(),p.getPromotion(),p.getPhotoDetails(),p.getTextDetails(),p.getPid()};
		
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//delete
	public int Delete(int pid) {
		int r = -1;
		String sql = "Delete from product where pid = ?";
		Object[]params = {pid};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//select pname
	//@Test
	public List<Product> SelectPname(String pname) {
		String sql = "select * from product where pname like" + "'%" + pname + "%'";
		Object params[] = {};
		ProductMapper mapper = new ProductMapper();
		List<Product> list = ju.executeQuery(sql, mapper, params);
		return list;
		
	}
	
	//find by id
	public Product findById(int pid) {
		Product d = new Product(); 
		List<Product> list = null;
		
		String sql = "select * from Product where pid = ?";
		ProductMapper dm = new ProductMapper();
		
		Object[]params = {pid};
		list = ju.executeQuery(sql, dm, params);
		if(!list.isEmpty()) {
			d = list.get(0);
		}
		return d;
	}
}