package com.hwua.dao;
/*
 * CarSqlMethod
 * 
 */

import java.util.List;

import com.hwua.jdbc.JdbcUtils;
import com.hwua.mapper.CarMapper;
import com.hwua.po.Car;

public class CarDao {
	public static JdbcUtils ju = new JdbcUtils();
	
	//select
	public List<Car> Select(){
		String sql = "select * from car";
		CarMapper mapper = new CarMapper();
		Object params[] = {};
		List<Car> list = ju.executeQuery(sql, mapper, params);
		return list;
	}
	
	//add
	public int Add(Car c) {
		int r = -1;
		String sql = "insert into car (cid,pid,userid,count)"
					+ " values(SEQ_CAR_CID.nextval,?,?,?)"; 
		Object params[] = {c.getPid(),c.getUserid(),c.getCount()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//update
	public int Update(Car c) {
		int r = -1;
		String sql = "update car set pid=101,userid=1006,count=2 "
					+ "where cid=01";
		Object params[] = {c.getPid(),c.getUserid(),c.getCount(),c.getCid()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//delete
	public int Delete(int cid) {
		int r = -1;
		String sql = "Delete from car where cid = ?";
		Object params[] = {cid};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	public List<Car> findById(int userid){
		String sql = "select * from car where userid = ?";
		CarMapper mapper = new CarMapper();
		Object params[] = {userid};
		
		List<Car> list = ju.executeQuery(sql, mapper, params);
		return list;
	}
	
	public Car findByCid(int cid) {
		Car c = new Car();
		List<Car>list = null;
		
		String sql = "select * from car where cid = ?";
		CarMapper cm = new CarMapper();
		Object[]params = {cid};
		
		list = ju.executeQuery(sql, cm, params);
		if(!list.isEmpty()) {
			c = list.get(0);
		}
		return c;
	}
}
