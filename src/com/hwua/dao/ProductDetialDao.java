package com.hwua.dao;
/*
 * ProductDetialSqlMethod
 * 
 */

import java.util.List;

import org.junit.Test;

import com.hwua.jdbc.JdbcUtils;
import com.hwua.mapper.ProductDetialMapper;
import com.hwua.mapper.ProductMapper;
import com.hwua.po.Productdetial;

public class ProductDetialDao {
	public static JdbcUtils ju = new JdbcUtils();
	
	//select
	public List<Productdetial> Select(){
		String sql = "select * from productdetial";
		ProductDetialMapper ddm = new ProductDetialMapper();
		Object params[] = {};
		List<Productdetial> list = ju.executeQuery(sql, ddm, params);
		return list;
		//System.out.println(list);
	}
	
	//Add
	public int Add(Productdetial p) {
		int r = -1;
		String sql = "insert into productdetial (pid,pdbtext,pphotos,ptextphotos) values(?,?,?,?)";
		Object params[] = {p.getPid(),p.getPdbtext(),p.getPphotos(),p.getPtextphotos()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//update
	public int Update(Productdetial p) {
		int r = -1;
		String sql = "update productdetial set pdbtext = ?,pphotos = ?,ptextphotos = ?"
					 + "where pid = ?";
		Object params[] = {p.getPdbtext(),p.getPphotos(),p.getPtextphotos(),p.getPid()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//delete
	public int Delete(int pid) {
		int r = -1;
		String sql = "delete from productdetial where pid = ?";
		Object params[] = {pid};
		r = ju.executeUpdate(sql, params);
		
		return r;
	}
}
