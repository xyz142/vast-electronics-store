package com.hwua.dao;
/*
 * EvaluateSqlMethod
 * 
 */

import java.util.List;

import com.hwua.jdbc.JdbcUtils;
import com.hwua.mapper.EvaluateMapper;
import com.hwua.po.Evaluate;

public class EvaluateDao {
	public static JdbcUtils ju = new JdbcUtils();
	
	//select
	//@org.junit.Test
	public List<Evaluate> Select(){
		String sql = "select * from Evaluate";
		Object params[] = {};
		EvaluateMapper mapper = new EvaluateMapper();
		List<Evaluate> list = ju.executeQuery(sql, mapper, params);
		return list;
		//System.out.println(list);
	}
	
	//add
	public int Add(Evaluate e) {
		int r = -1;
		String sql = "insert into evaluate(eid,userid,username,etext,ecode,pid) "
					+ "values(?,?,?,?,?,?)";
		Object params[] = {e.getEid(),e.getUserid(),e.getUsername(),e.getEtext(),e.getEcode(),e.getPid()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//update
	public int Update(Evaluate e) {
		int r = -1;
		String sql = "update evaluate set userid=?,username=?,etext=?,ecode=?,pid=? "
					+ "where eid = ?";
		Object params[] = {e.getUserid(),e.getUsername(),e.getEtext(),e.getEcode(),e.getPid(),e.getEid()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//delete
	public int Delete(int eid) {
		int r = -1;
		String sql = "Delete from evaluate where eid = ?";
		Object params[] = {eid};
		r = ju.executeUpdate(sql, params);
		return r;
	}
}
