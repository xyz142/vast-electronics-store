package com.hwua.dao;
/*
 * SiteSqlMethod
 * 
 */

import java.util.List;

import com.hwua.jdbc.JdbcUtils;
import com.hwua.mapper.SiteMapper;
import com.hwua.po.Site;

public class SiteDao {
	public static JdbcUtils ju = new JdbcUtils();
	
	//select
	public List<Site> Select(){
		String sql = "select * from site";
		Object params[] = {};
		SiteMapper mapper = new SiteMapper();
		
		List<Site> list = ju.executeQuery(sql, mapper, params);
		return list;
	}
	
	//Add
	public int Add(Site s) {
		int r = -1;
		String sql = "insert into site(sid,userid,location,locationtext) "
					+ "values(?,?,?,?)";
		Object params[] = {s.getSid(),s.getUserid(),s.getLocation(),s.getLocationtext()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//Update
	public int Update(Site s) {
		int r = -1;
		String sql = "Update site set userid=?,location=?,locationtext=?"
					+ " where sid = ?";
		Object params[] = {s.getUserid(),s.getLocation(),s.getLocationtext(),s.getSid()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//Delete
	public int Delete(int sid) {
		int r = -1;
		String sql = "Delete from site where sid = ?";
		Object params[] = {sid};
		r = ju.executeUpdate(sql, params);
		return r;
	}
}
