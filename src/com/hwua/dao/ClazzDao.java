package com.hwua.dao;
/*
 * ClazzSqlMethod
 * 
 */

import java.util.List;

import com.hwua.jdbc.JdbcUtils;
import com.hwua.mapper.ClazzMapper;
import com.hwua.po.Clazz;

public class ClazzDao {
	public static JdbcUtils ju = new JdbcUtils();
	
	//select
	//@org.junit.Test
	public List<Clazz> Select(){
		String sql = "select * from clazz";
		Object params[] = {};
		ClazzMapper mapper = new ClazzMapper();
		List<Clazz> list = ju.executeQuery(sql, mapper, params);
		return list;
		//System.out.println(list);
	}
	
	//add
	public int Add(Clazz c) {
		int r = -1;
		String sql = "insert into clazz (cid,cname)"+"values(?,?)";
		Object params[] = {c.getCid(),c.getCname()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//update
	public int Update(Clazz c) {
		int r = -1;
		String sql = "update clazz set cname = ?" + "where cid = ?";
		Object params[] = {c.getCname(),c.getCid()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//delete
	public int Delete(int cid) {
		int r = -1;
		String sql = "delete from clazz where cid = ?";
		Object params[] = {cid};
		r = ju.executeUpdate(sql, params);
		return r;
	}
}
