package com.hwua.dao;

import java.util.List;

import com.hwua.jdbc.JdbcUtils;
import com.hwua.mapper.CollectsMapper;
import com.hwua.po.Collects;

public class CollectsDao {
	public static JdbcUtils ju = new JdbcUtils();
	
	//Select
	public List<Collects> Select(){
		String sql = "select * from collects";
		Object params[] = {};
		CollectsMapper mapper = new CollectsMapper();
		List<Collects> list = ju.executeQuery(sql, mapper, params);
		return list;
	}
	
	//add
	public int Add(Collects c) {
		int r = -1;
		String sql = "insert into collects (cid,pid,userid,count) "
					+ "values(?,?,?,?)";
		Object params[] = {c.getCid(),c.getPid(),c.getUserid(),c.getCount()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//update
	public int Update(Collects c) {
		int r = -1;
		String sql = "update collects set pid=?,userid=?,count=? "
					+ "where cid=?";
		Object params[] = {c.getPid(),c.getUserid(),c.getCount(),c.getCid()};
		return r;
	}
	
	//delete
	public int Delete(int cid) {
		int r = -1;
		String sql = "Delete from collects where cid = ?";
		Object params[] = {cid};
		r = ju.executeUpdate(sql, params);
		return r;
	}
}
