package com.hwua.dao;

import java.util.List;

import org.junit.Test;

import com.hwua.jdbc.JdbcUtils;
import com.hwua.mapper.UseMapper;
import com.hwua.po.Use;

/*
 * UseSqlMethod
 * 
 */
public class UseDao {
	
	public static JdbcUtils ju = new JdbcUtils();
	
	//user array
	//@Test
	public List<Use> UseArray(){
		String sql = "select * from use";
		UseMapper um = new UseMapper();
		Object[]params = {};
		List<Use> list = ju.executeQuery(sql, um, params);
		return list;
		//System.out.println(list);
	}
	
	//registered	text photo age sex location usercode
	public int registered(Use u) {
		int r = -1;
		String sql = "insert into use(NUM,NAME,PASSWORD,ISROOT)" 
					+ "values(SEQ_USE_NUM.nextval,?,?,?)";
		Object[]params = {u.getName(),u.getPassword(),u.getIsRoot()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//Update usermessage   num为自动生成
	public int UpdateMassage(Use u) {
		int r = -1;
		String sql = "update use set name=?,text=?,age=?,sex=?,location=? "
				+ "where num=?";
		Object[]params = {u.getName(),u.getText(),u.getAge(),u.getSex(),u.getLocation(),u.getNum()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
	
	//update userphoto
	public int UpdatePhoto(Use u) {
		int r = -1;
		String sql = "update use set photo = ? where num = ?";
		Object[]params = {u.getPhoto(),u.getNum()};
		r = ju.executeUpdate(sql, params);
		return r;
	}
}
