package com.hwua.jdbc;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import com.hwua.mapper.IMapper;

/*
 * 写入配置文件，实现连接方法，增删改查方法
 * 
 */
public class JdbcUtils {
	private static String DRIVER;
	private static String URL;
	private static String USER;
	private static String PASS;
	
	static {
		Properties pro = new Properties();
		InputStream in = JdbcUtils.class.getClassLoader().getResourceAsStream("com\\hwua\\jdbc\\db.properties");
		
		try {
			pro.load(in);
			DRIVER = pro.getProperty("DRIVER");
			URL = pro.getProperty("URL");
			USER = pro.getProperty("USER");
			PASS = pro.getProperty("PASS");
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	//连接方法
	public static Connection getConnection() throws Exception {
		Connection conn = null;
		
		Class.forName(DRIVER);
		conn = DriverManager.getConnection(URL, USER, PASS);
		return conn;
	}
	/*
	 * 实现增删改查方法
	 * 	
	 */
	//增删改
	public int executeUpdate(String sql , Object[]params) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = JdbcUtils.getConnection();
			ps = conn.prepareStatement(sql);
			if(params != null) {
				for (int i = 0; i < params.length; i++) {
					ps.setObject(i+1 , params[i]);
				}
			}
			return ps.executeUpdate();
			//System.out.println(result == 1? "修改:"+ result +"条数据成功" : "修改失败");
		} catch (Exception e) {
			
			e.printStackTrace();
		}finally {
			if(ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
			}
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
			}
		}
		return 0;
	}
	//查询
	public List executeQuery(String sql,IMapper mapper,Object[] params) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List list = null;
		try {
			conn = JdbcUtils.getConnection();
			ps = conn.prepareStatement(sql);
			if(params != null) {
				for (int i = 0; i < params.length; i++) {
					ps.setObject(i+1 , params[i]);
				}
			}
			rs = ps.executeQuery();
			list = mapper.Mapper(rs);
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
			}
			if(ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
			}
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
	public int executeQueryJvHe(String sql,Object []params){		
		Connection conn=null;
		PreparedStatement stm=null;
		ResultSet rs=null;
		try {
			conn=JdbcUtils.getConnection();
			stm=conn.prepareStatement(sql);
			if (params!=null) {
				for (int i = 0; i < params.length; i++) {
					stm.setObject(i+1, params[i]);
				}
			}
			rs=stm.executeQuery();
			rs.next();
			int result=rs.getInt(1);
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
}
