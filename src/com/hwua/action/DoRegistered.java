package com.hwua.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hwua.dao.UseDao;
import com.hwua.po.Use;

/**
 * Servlet implementation class DoRegistered
 */
@WebServlet("/DoRegistered")
public class DoRegistered extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoRegistered() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		//注册不为空拦截
		if(username.equals("") && password.equals("")) {
			System.out.println("触发非空验证");
			response.sendRedirect("Registered.jsp");
			return;
			
		}else if(username.equals("")) {
			System.out.println("触发非空验证");
			response.sendRedirect("Registered.jsp");
			return;
			
		}else if(password.equals("")) {
			System.out.println("触发非空验证");
			response.sendRedirect("Registered.jsp");
			return;
		}
		
		//进行注册判断
		boolean bRe = true;
		UseDao ud = new UseDao();
		List<Use> list = ud.UseArray();
		for (int i = 0; i < list.size(); i++) {
			if(username.equals(list.get(i).getName())) {
				bRe = false;
				System.out.println("提示:“"+username+"”存在注册失败");
				request.setAttribute("error", "用户“" + username + "”已存在,换个用户名试试!");
				request.getRequestDispatcher("Registered.jsp").forward(request, response);
			}
		}if(bRe) {
			
			System.out.println("system:通过注册验证,注册的用户:" + username + "/" + password);
			Use u = new Use();
			u.setName(username);
			u.setPassword(password);
			u.setIsRoot("n");
			ud.registered(u);
			
			request.setAttribute("registername", "用户“" + username + "”注册成功! 重新登录吧!");
			request.getRequestDispatcher("Registered.jsp").forward(request, response);
		}
	}
}
