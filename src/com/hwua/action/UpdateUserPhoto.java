package com.hwua.action;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.hwua.dao.UseDao;
import com.hwua.po.Use;


/**
 * Servlet implementation class UpdateUserPhoto
 */
@WebServlet("/UpdateUserPhoto")
public class UpdateUserPhoto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateUserPhoto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//上传
		try {
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if(isMultipart) {//判断前台form表单是否有此属性
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			List<FileItem> items = upload.parseRequest(request);
			Iterator<FileItem> iter = items.iterator();
			
			while(iter.hasNext()) {
				FileItem item = iter.next();
				String fileName = item.getName();//文件名
				System.out.println("上传的文件名：" + fileName);
				
				//上传位置
				String path = "C:/Users/dell/Desktop/eElectronics/img";
				File file = new File(path + "/" + fileName);
				item.write(file);
				System.out.println(fileName + "：上传成功");
				
				UseDao ud = new UseDao();
				HttpSession session = request.getSession();
				Use use = (Use)session.getAttribute("usemessage");
				int num = use.getNum();
				Use u = new Use();
				u.setPhoto(fileName);
				u.setNum(num);
				int r = -1;
				r = ud.UpdatePhoto(u);
				if(r > 0) {
					System.out.println("修改用户头像成功");
					request.setAttribute("UpdatePhotoSuccess","用户“" + use.getNum() + "”修改信息成功,请重新登陆");
					request.getRequestDispatcher("UseMessage.jsp").forward(request, response);
				}
				return;
			}
		}
		
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
