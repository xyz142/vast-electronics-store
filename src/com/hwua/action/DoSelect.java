package com.hwua.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hwua.dao.ProductDao;
import com.hwua.po.Product;

/**
 * Servlet implementation class DoSelect
 */
@WebServlet("/DoSelect")
public class DoSelect extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoSelect() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String selectname = request.getParameter("selectname");
		
		boolean bSe = true;
		ProductDao pd = new ProductDao();
		
		List<Product> list = pd.SelectPname(selectname);//模糊查询
		
		for (int i = 0; i < list.size(); i++) {
			if(!list.isEmpty()) {
				bSe = false;
				System.out.println(list.size());
				request.setAttribute("selectlist", list);
				//request.getRequestDispatcher("SelectShop.jsp").forward(request, response);
				request.getRequestDispatcher("Test.jsp").forward(request, response);
			}
		}if(bSe) {
			request.setAttribute("error", "没有找到“" + selectname + "”有关的信息!");
			request.getRequestDispatcher("Shop.jsp").forward(request, response);
		}
	}
}
