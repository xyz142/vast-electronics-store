package com.hwua.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hwua.dao.CarDao;
import com.hwua.po.Car;
import com.hwua.po.Use;

import net.sf.json.JSONObject;

/**
 * Servlet implementation class DoAddCart
 */
@WebServlet("/DoAddCart")
public class DoAddCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoAddCart() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		int pid = Integer.parseInt(request.getParameter("pid"));
		HttpSession session = request.getSession();
		Use use = (Use)session.getAttribute("usemessage");
		System.out.println("Cart��ȡ:" + pid + "/" + use.getNum());
		
		CarDao cd = new CarDao();
		Car c = new Car();
		c.setPid(pid);
		c.setUserid(use.getNum());
		c.setCount(1);
		
		int r = -1;
		r = cd.Add(c);
	
		System.out.println(r == -1? "ʧ��":"�ɹ�");
		response.sendRedirect("Shop.jsp");
	}
}
