package com.hwua.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hwua.dao.UseDao;
import com.hwua.po.Use;

/**
 * Servlet implementation class DoLogin
 */
@WebServlet("/DoLogin")
public class DoLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		if(username.equals("") && password.equals("")) {
			System.out.println("触发非空验证");
			response.sendRedirect("Login.jsp");
			return;
			
		}else if(username.equals("")) {
			System.out.println("触发非空验证");
			response.sendRedirect("Login.jsp");
			return;
			
		}else if(password.equals("")) {
			System.out.println("触发非空验证");
			response.sendRedirect("Login.jsp");
			return;
		}
		
		HttpSession session = request.getSession();
		UseDao ud = new UseDao();
		List<Use> list = ud.UseArray();
		boolean b = true;
		for (int i = 0; i < list.size(); i++) {
			if(username.equals(list.get(i).getName()) 
			&& password.equals(list.get(i).getPassword()) && list.get(i).getIsRoot().equals("n")) {
				b = false;
				System.out.println("登录:" + username + "/" + password);
				session.setAttribute("username", username);
				session.setAttribute("usemessage", list.get(i));
				request.getRequestDispatcher("First.jsp").forward(request, response);
					
			}if(username.equals(list.get(i).getName()) 

					&& password.equals(list.get(i).getPassword()) && list.get(i).getIsRoot().equals("y")) {
					b = false;
					System.out.println("为管理员root登录");
					session.setAttribute("admin",username );
					request.getRequestDispatcher("AdminLog.jsp").forward(request, response);
			}
		}if(b) {
			request.setAttribute("error", "ERROR:“" + username + "” 输入错误的账户或密码");
			request.getRequestDispatcher("Login.jsp").forward(request, response);
		}
	}
}
