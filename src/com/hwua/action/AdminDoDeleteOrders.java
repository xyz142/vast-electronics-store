package com.hwua.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hwua.dao.OrdersDao;

/**
 * Servlet implementation class AdminDoDeleteOrders
 */
@WebServlet("/AdminDoDeleteOrders")
public class AdminDoDeleteOrders extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminDoDeleteOrders() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int oid = Integer.parseInt(request.getParameter("oid"));
		PrintWriter out = response.getWriter();
		OrdersDao od = new OrdersDao();
		int r = -1;
		r = od.Delete(oid);
		if(r > 0) {
			System.out.println("ɾ�������ɹ�");
			request.getRequestDispatcher("AdminDoShowOrders").forward(request, response);
		}else {
			out.print("ɾ������ʧ��");
		}
	}
}
