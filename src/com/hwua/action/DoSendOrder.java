package com.hwua.action;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hwua.dao.CarDao;
import com.hwua.dao.OrdersDao;
import com.hwua.dao.ProductDao;
import com.hwua.po.Car;
import com.hwua.po.Orders;
import com.hwua.po.Product;
import com.hwua.po.Use;

/**
 * Servlet implementation class DoSendOrder
 */
@WebServlet("/DoSendOrder")
public class DoSendOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoSendOrder() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String orderlist = request.getParameter("orderlist");
		String[] cids = orderlist.split(",");
		HttpSession session = request.getSession();
		Use use = (Use)session.getAttribute("usemessage");
		int userid = use.getNum();
		String site = use.getLocation();
		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String orderdate = sdf.format(date);
		CarDao cd = new CarDao();
		OrdersDao od = new OrdersDao();
		
		//获取购物车pid对应的属性，根据pid查询商品详情信息
		ProductDao pd = new ProductDao();
		
		//添加订单 ：订单编号，创建日期，发货状态，商品id，*用户地址，*用户编号  
		//删除购物车 ，跳转至支付页面
		for(String cid:cids) {
			Orders o = new Orders();
			Car c = cd.findByCid(new Integer(cid));
			
			Product p = pd.findById(c.getPid());
			c.setProduct(p);
			if(c.getProduct().getPname().equals("") || c.getProduct().getPrice() == 0.0){
				System.out.println("触发拦截");
				return;
			}
			System.out.println("商品：" + c.getProduct().getPname() + "价值：" +  c.getProduct().getPrice());
			
			o.setOdate(orderdate);
			o.setOboolean("待发货");
			o.setPid(c.getPid());
			o.setSiteid(site);
			o.setUserid(userid);
			//添加至订单
			od.Add(o);
			
			//删除购物车订单信息
			int r = cd.Delete(new Integer(cid));
			System.out.println(r == -1? "后台购物车删除失败":"后台购物车删除成功");
			
			request.getRequestDispatcher("Cart.jsp").forward(request, response);
		}
	}
}
