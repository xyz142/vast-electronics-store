package com.hwua.action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hwua.dao.ProductDao;
import com.hwua.po.Product;

/**
 * Servlet implementation class AdminDoAddProduct
 */
@WebServlet("/AdminDoAddProduct")
public class AdminDoAddProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminDoAddProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String pname = request.getParameter("pname");
		String pcount = request.getParameter("pcount");
		String price = request.getParameter("price");
		String oldprice = request.getParameter("oldprice");
		String ptext = request.getParameter("ptext");
		String uid = request.getParameter("uid");
		String pdate = request.getParameter("pdate");
		String photo = request.getParameter("photo");
		String pboolean = request.getParameter("pboolean");
		String locations = request.getParameter("locations");
		String clazzid = request.getParameter("clazzid");
		String promotion = request.getParameter("promotion");
		String photodetails = request.getParameter("photodetails");
		String textdetails = request.getParameter("textdetails");
		
		ProductDao pd = new ProductDao();
		Product p = new Product();
		
		p.setPname(pname);
		p.setPcount(new Integer(pcount));
		p.setPrice(new Double(price));
		p.setOldprice(new Double(oldprice));
		p.setPtext(ptext);
		p.setUids(new Integer(uid));
		p.setPdate(pdate);
		p.setPhoto(photo);
		p.setPboolean(pboolean);
		p.setLocations(locations);
		p.setClazzid(new Integer(clazzid));
		p.setPromotion(promotion);
		p.setPhotoDetails(photodetails);
		p.setTextDetails(textdetails);
		
		int r = -1;
		r = pd.Add(p);
		if(r > 0) {
			System.out.println("add success");
			request.getRequestDispatcher("AdminDoShowProduct").forward(request, response);
			
		}else {
			System.out.println("add error");
		}
	}
}
