package com.hwua.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hwua.dao.OrdersDao;
import com.hwua.po.Orders;
import com.hwua.po.Use;

/**
 * Servlet implementation class CheckMyOrders
 */
@WebServlet("/CheckMyOrders")
public class CheckMyOrders extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckMyOrders() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//展示所登陆的用户的订单状态信息
		HttpSession session = request.getSession();
		Use use = (Use)session.getAttribute("usemessage");
		int userid = use.getNum();
		
		OrdersDao od = new OrdersDao();
		//获取了登录userid所指定的订单信息集合
		List<Orders> list = od.findByUserid(userid);
		session.setAttribute("userorder", list);
		request.getRequestDispatcher("MyOrders.jsp").forward(request, response);
	}
}
