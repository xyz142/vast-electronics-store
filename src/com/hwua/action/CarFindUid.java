package com.hwua.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hwua.dao.CarDao;
import com.hwua.dao.ProductDao;
import com.hwua.po.Car;
import com.hwua.po.Product;
import com.hwua.po.Use;

import net.sf.json.JSONObject;

/**
 * Servlet implementation class CarFindUid
 */
@WebServlet("/CarFindUid")
public class CarFindUid extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CarFindUid() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession();
		Use use = (Use)session.getAttribute("usemessage");
		
		int userid = use.getNum();
		System.out.println(userid);
		CarDao cd = new CarDao();
		
		List<Car> list = cd.findById(userid);
		if(list.isEmpty()) {
			return;
		}
		System.out.println("��ȡ������" + list);
		ProductDao pd = new ProductDao();
		
		for(Car car:list) {
			Product product = pd.findById(car.getPid());
			//System.out.println(product);
			car.setProduct(product);
		}
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("list", list);
		JSONObject jsonmap = JSONObject.fromObject(map);
		//System.out.println(jsonmap);
		out.print(jsonmap);
	}
}
