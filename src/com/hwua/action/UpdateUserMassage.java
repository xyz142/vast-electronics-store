package com.hwua.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hwua.dao.UseDao;
import com.hwua.po.Use;

/**
 * Servlet implementation class UpdateUserMassage
 */
@WebServlet("/UpdateUserMassage")
public class UpdateUserMassage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateUserMassage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String text = request.getParameter("text");
		int age = Integer.parseInt(request.getParameter("age"));
		String sex = request.getParameter("sex");
		String location = request.getParameter("location");
		HttpSession session = request.getSession();
		Use use = (Use)session.getAttribute("usemessage");
		int num = use.getNum();
		
		UseDao ud = new UseDao();
		Use u = new Use();
		u.setName(name);
		u.setText(text);
		u.setAge(age);
		u.setSex(sex);
		u.setLocation(location);
		u.setNum(num);
		
		int r = -1;
		r = ud.UpdateMassage(u);
		if(r > 0) {
			System.out.println("修改用户信息成功");
			request.setAttribute("UpdateSuccess","用户“" + use.getNum() + "”修改信息成功,请重新登陆");
			request.getRequestDispatcher("UseMessage.jsp").forward(request, response);
		}
	}
}
